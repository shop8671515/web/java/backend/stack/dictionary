package pl.com.bit.dictionary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.security.servlet.ManagementWebSecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.scheduling.annotation.EnableScheduling;
import pl.com.bit.dictionary.event.DictionaryUpdateStream;

@EnableBinding({DictionaryUpdateStream.Source.class, DictionaryUpdateStream.Sink.class})
@EnableScheduling
@EnableDiscoveryClient
@SpringBootApplication(
    exclude = {SecurityAutoConfiguration.class, ManagementWebSecurityAutoConfiguration.class})
public class DictionaryServiceApplicationBootstrap {

  public static void main(String[] args) {
    SpringApplication.run(DictionaryServiceApplicationBootstrap.class, args);
  }
}
