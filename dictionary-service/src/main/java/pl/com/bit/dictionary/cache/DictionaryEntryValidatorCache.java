package pl.com.bit.dictionary.cache;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.neovisionaries.i18n.LanguageCode;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.annotation.Nullable;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Synchronized;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Streamable;
import pl.com.rszewczyk.stack.dictionary.api.DictionaryEntriesApi;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite;

@RequiredArgsConstructor
@CacheConfig(cacheManager = DictionaryLocalCacheValidatorAutoConfiguration.CACHE_MANAGER_BEAN)
public class DictionaryEntryValidatorCache {
  private final DictionaryEntriesApi dictionaryEntriesApi;

  public static final String TRANSLATION_CACHE_NAME = "validatorTranslationCache";

  @Synchronized
  @Cacheable(TRANSLATION_CACHE_NAME)
  public Map<LanguageCode, Map<String, Entry>> getExistingKeys(@NotBlank String dictionaryName) {
    List<DictionaryEntryLite> entries =
        Optional.ofNullable(
                dictionaryEntriesApi
                    .getDictionaryEntries(
                        new DictionaryEntryFilter().dictionaryName(dictionaryName),
                        Pageable.unpaged())
                    .getBody())
            .map(Streamable::toList)
            .orElse(Collections.emptyList());
    Map<LanguageCode, Map<String, Entry>> map = Maps.newHashMap();
    entries.forEach(entry -> putEntryInMap(entry, map));
    return map;
  }

  private static void putEntryInMap(
      @NotNull @Valid DictionaryEntryLite entryLite,
      @NotNull Map<LanguageCode, Map<String, Entry>> map) {
    LanguageCode languageCode = LanguageCode.getByCode(entryLite.getLanguage());
    String key = entryLite.getKey();

    map.computeIfAbsent(languageCode, k -> Maps.newHashMap());
    Map<String, Entry> languageMap = map.get(languageCode);

    languageMap.computeIfAbsent(key, k -> new Entry(key, Sets.newHashSet()));
  }

  @CacheEvict(TRANSLATION_CACHE_NAME)
  public void invalidate(@Nullable String ignoredDictionaryName) {}

  @CacheEvict(value = TRANSLATION_CACHE_NAME, allEntries = true)
  public void invalidateAll() {}

  @Getter
  @AllArgsConstructor
  public static class Entry {
    private final String key;
    private final Set<String> transitionKeys;
  }
}
