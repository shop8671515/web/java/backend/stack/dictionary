package pl.com.bit.dictionary.cache;

import com.github.benmanes.caffeine.cache.Caffeine;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.com.bit.dictionary.event.DictionaryUpdateStream;
import pl.com.rszewczyk.stack.dictionary.api.DictionaryEntriesApi;

@EnableCaching
@EnableBinding(DictionaryUpdateStream.Sink.class)
@Configuration
@ConditionalOnProperty(
    name = "rszewczyk.dictionary.validator.enabled",
    havingValue = "true",
    matchIfMissing = true)
public class DictionaryLocalCacheValidatorAutoConfiguration {
  public static final String CACHE_MANAGER_BEAN = "dictionaryValidatorCacheManager";

  @Bean(CACHE_MANAGER_BEAN)
  public CacheManager cacheManager(
      @Value("${rszewczyk.dictionary.validator.cache.size:10}") long cacheSize) {
    CaffeineCache cache =
        new CaffeineCache(
            DictionaryEntryValidatorCache.TRANSLATION_CACHE_NAME,
            Caffeine.newBuilder()
                .expireAfterWrite(5, TimeUnit.MINUTES)
                .maximumSize(cacheSize)
                .build());
    SimpleCacheManager manager = new SimpleCacheManager();
    manager.setCaches(Collections.singletonList(cache));
    return manager;
  }

  @Bean
  public DictionaryEntryValidatorCache dictionaryEntryValidatorCache(@NotNull DictionaryEntriesApi dictionaryEntriesApi) {
    return new DictionaryEntryValidatorCache(dictionaryEntriesApi);
  }
}
