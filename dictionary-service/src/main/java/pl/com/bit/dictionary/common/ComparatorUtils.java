package pl.com.bit.dictionary.common;

import java.util.Objects;
import java.util.function.Supplier;

public class ComparatorUtils {

    public static <T, Y, X extends Throwable> boolean equalsOrThrow(T val1, Y val2, Supplier<? extends X> supplier) throws X {
        if ((Objects.isNull(val1) && Objects.isNull(val2) || (Objects.nonNull(val1) && val1.equals(val2)))) {
            return true;
        }
        throw supplier.get();
    }
}
