package pl.com.bit.dictionary.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.codec.ErrorDecoder;
import javax.validation.constraints.NotNull;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.com.bit.common.api.support.feign.SpringPageDeserializeModule;

@Configuration
public class BeanConfig {

  @Bean
  @ConditionalOnMissingBean
  public SpringPageDeserializeModule feignPageableJacksonModule() {
    return new SpringPageDeserializeModule();
  }

  @Bean
  public ErrorDecoder errorDecoder(@NotNull ObjectMapper objectMapper) {
    return new DictionaryErrorDecoder(objectMapper);
  }
}
