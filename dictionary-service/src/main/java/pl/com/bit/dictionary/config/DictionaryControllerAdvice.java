package pl.com.bit.dictionary.config;

import javax.persistence.RollbackException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.JDBCException;
import org.hibernate.TransactionException;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.NativeWebRequest;
import pl.com.bit.common.ErrorCode;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.advice.Advice;

@Slf4j
@Order(1)
@RestControllerAdvice
public class DictionaryControllerAdvice extends Advice {

  @ExceptionHandler
  public ResponseEntity<Object> handle(JDBCException exception, NativeWebRequest request) {
    return create(
        HttpStatus.INTERNAL_SERVER_ERROR,
        exception,
        request,
        ProblemLogLevel.ERROR,
        error(ErrorCode.INTERNAL_COMMUNICATION_EXCEPTION.name()));
  }

  @ExceptionHandler
  public ResponseEntity<Object> handle(RollbackException exception, NativeWebRequest request) {
    return create(
        HttpStatus.INTERNAL_SERVER_ERROR,
        exception,
        request,
        ProblemLogLevel.ERROR,
        error(ErrorCode.INTERNAL_COMMUNICATION_EXCEPTION.name()));
  }

  @ExceptionHandler
  public ResponseEntity<Object> handle(TransactionException exception, NativeWebRequest request) {
    log.error("Transaction exception", exception);
    return create(
        HttpStatus.INTERNAL_SERVER_ERROR,
        exception,
        request,
        ProblemLogLevel.ERROR,
        error("TRANSACTION_EXCEPTION"));
  }
}
