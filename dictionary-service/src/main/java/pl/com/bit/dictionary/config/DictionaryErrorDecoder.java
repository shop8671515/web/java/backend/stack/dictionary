package pl.com.bit.dictionary.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Optional;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.EnumUtils;
import pl.com.bit.common.feign.CommonErrorDecoder;
import pl.com.bit.dictionary.exception.DictionaryAlreadyExistsException;
import pl.com.bit.dictionary.exception.DictionaryEntryNotFoundException;
import pl.com.bit.dictionary.exception.DictionaryNotFoundException;
import pl.com.bit.dictionary.exception.SystemDictionaryDeletionException;
import pl.com.bit.dictionary.exception.SystemDictionaryEntryDeletionException;
import pl.com.bit.dictionary.exception.SystemDictionaryEntryModificationException;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class DictionaryErrorDecoder extends CommonErrorDecoder {

  public DictionaryErrorDecoder(@NotNull ObjectMapper objectMapper) {
    super(
        objectMapper,
        (code, details) -> {
          if (EnumUtils.isValidEnum(ErrorCode.class, code)) {
            return tryDecode(ErrorCode.valueOf(code), details).map(RuntimeException.class::cast);
          }
          return Optional.empty();
        });
  }

  private static Optional<RuntimeException> tryDecode(
      @NotNull ErrorCode code, @NotBlank String message) {
    return Optional.ofNullable(decode(code, message));
  }

  @Nullable
  private static RuntimeException decode(@NotNull ErrorCode code, @NotBlank String message) {
    switch (code) {
      case DICTIONARY_NOT_FOUND:
        return new DictionaryNotFoundException(message);
      case DICTIONARY_ALREADY_EXISTS:
        return new DictionaryAlreadyExistsException(message);
      case SYSTEM_DICTIONARY_ATTEMPT_DELETION:
        return new SystemDictionaryDeletionException(message);
      case DICTIONARY_ENTRY_NOT_FOUND:
        return new DictionaryEntryNotFoundException(message);
      case SYSTEM_DICTIONARY_ENTRY_ATTEMPT_UPDATE:
        return new SystemDictionaryEntryModificationException(message);
      case SYSTEM_DICTIONARY_ENTRY_DELETION_ATTEMPT:
        return new SystemDictionaryEntryDeletionException(message);
      default:
        return null;
    }
  }
}
