package pl.com.bit.dictionary.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = "pl.com.bit.dictionary")
@EnableJpaRepositories("pl.com.bit.dictionary")
public class EntityConfig {}
