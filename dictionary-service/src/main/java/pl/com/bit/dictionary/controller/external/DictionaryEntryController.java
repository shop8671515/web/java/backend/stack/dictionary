package pl.com.bit.dictionary.controller.external;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import static org.springframework.http.ResponseEntity.ok;
import org.springframework.stereotype.Controller;
import pl.com.bit.common.security.api.AccountIdentity;
import static pl.com.bit.common.security.api.UserContext.getIdentityOrThrow;
import pl.com.bit.dictionary.service.external.DictionaryEntryManagementService;
import pl.com.rszewczyk.stack.dictionary.api.DictionaryEntriesApi;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite;

@Slf4j
@Controller
@RequiredArgsConstructor
public class DictionaryEntryController implements DictionaryEntriesApi {
    private final DictionaryEntryManagementService dictionaryEntryManagementService;

    @Override
    public ResponseEntity<Page<DictionaryEntryLite>> getDictionaryEntries(DictionaryEntryFilter filter, Pageable pageable) {
        log.trace("Getting all dictionary entries {filter: {}}", filter);
        AccountIdentity accountIdentity = getIdentityOrThrow();
        Page<DictionaryEntryLite> page = dictionaryEntryManagementService.getDictionaryEntries(filter, pageable, accountIdentity);
        log.debug("Got all dictionary entries by dictionary id {number: {}}", page.getTotalElements());
        return ok(page);
    }
}
