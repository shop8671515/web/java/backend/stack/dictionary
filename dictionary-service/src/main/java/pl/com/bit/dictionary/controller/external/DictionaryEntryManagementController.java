package pl.com.bit.dictionary.controller.external;

import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static pl.com.bit.common.security.api.UserContext.getAccountIdentity;
import static pl.com.bit.common.security.api.UserContext.getIdentityOrThrow;

import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.com.bit.common.security.api.AccountIdentity;
import pl.com.bit.dictionary.service.external.DictionaryEntryManagementService;
import pl.com.rszewczyk.stack.dictionary.api.TranslationManagementsApi;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryUpdateRequest;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DictionaryEntryManagementController implements TranslationManagementsApi {
  private final DictionaryEntryManagementService dictionaryEntryManagementService;

  @Override
  public ResponseEntity<DictionaryEntryDetails> createDictionaryEntry(
      @Valid DictionaryEntryCreateRequest dictionaryEntryCreateRequest) {
    log.trace("Creating dictionary entry {request: {}}", dictionaryEntryCreateRequest);
    AccountIdentity identity = getIdentityOrThrow();
    DictionaryEntryDetails details =
        dictionaryEntryManagementService.createDictionaryEntry(
            dictionaryEntryCreateRequest, identity);
    log.debug("Created dictionary entry {entryId: {}}", details.getId());
    return new ResponseEntity<>(details, HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<Void> deleteDictionaryEntry(UUID dictionaryEntryId) {
    log.trace("Deleting dictionary entry {entryId: {}}", dictionaryEntryId);
    AccountIdentity identity = getIdentityOrThrow();
    dictionaryEntryManagementService.deleteDictionaryEntry(dictionaryEntryId, identity);
    log.debug("Deleted dictionary entry");
    return noContent().build();
  }

  @Override
  public ResponseEntity<DictionaryEntryDetails> getDictionaryEntry(UUID dictionaryEntryId) {
    log.trace("Getting dictionary entry {entryId: {}}", dictionaryEntryId);
    AccountIdentity accountIdentity = getAccountIdentity();
    DictionaryEntryDetails details =
        dictionaryEntryManagementService.getDictionaryEntry(dictionaryEntryId, accountIdentity);
    log.debug("Got dictionary entry {entryId: {}}", details.getId());
    return ok(details);
  }

  @Override
  public ResponseEntity<DictionaryEntryDetails> updateDictionaryEntry(
      UUID dictionaryEntryId, @Valid DictionaryEntryUpdateRequest dictionaryEntryUpdateRequest) {
    log.trace(
        "Updating dictionary entry {entryId: {}, request: {}}",
        dictionaryEntryId,
        dictionaryEntryUpdateRequest);
    AccountIdentity accountIdentity = getAccountIdentity();
    DictionaryEntryDetails details =
        dictionaryEntryManagementService.updateDictionaryEntry(
            dictionaryEntryId, dictionaryEntryUpdateRequest, accountIdentity);
    log.debug("Updated dictionary entry {entryId: {}}", details.getId());
    return ok(details);
  }
}
