package pl.com.bit.dictionary.controller.external;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import static org.springframework.http.ResponseEntity.noContent;
import static org.springframework.http.ResponseEntity.ok;
import static pl.com.bit.common.security.api.UserContext.getIdentityOrThrow;

import java.util.UUID;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.com.bit.common.security.api.AccountIdentity;
import pl.com.bit.dictionary.service.external.DictionaryManagementService;
import pl.com.rszewczyk.stack.dictionary.api.DictionariesManagementsApi;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionarySnapshot;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryUpdateRequest;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DictionaryManagementController implements DictionariesManagementsApi {
  private final DictionaryManagementService dictionaryManagementService;

  @Override
  public ResponseEntity<DictionaryDetails> createDictionary(
      @Valid DictionaryCreateRequest dictionaryCreateRequest) {
    log.trace("Creating dictionary {request: {}}", dictionaryCreateRequest);
    AccountIdentity identity = getIdentityOrThrow();
    DictionaryDetails details =
        dictionaryManagementService.createDictionary(dictionaryCreateRequest, identity);
    log.debug("Created dictionary {dictionaryId: {}}", details.getId());
    return new ResponseEntity<>(details, HttpStatus.CREATED);
  }

  @Override
  public ResponseEntity<Void> deleteDictionary(UUID dictionaryId) {
    log.trace("Deleting dictionary {dictionaryId: {}}", dictionaryId);
    AccountIdentity identity = getIdentityOrThrow();
    dictionaryManagementService.deleteDictionary(dictionaryId, identity);
    log.debug("Deleted dictionary");
    return noContent().build();
  }

  @Override
  public ResponseEntity<Page<DictionarySnapshot>> findDictionaries(@Valid DictionaryFilter dictionaryFilter, Pageable pageable) {
    log.trace("Finding dictionaries {filter: {}}", dictionaryFilter);
    AccountIdentity identity = getIdentityOrThrow();
    Page<DictionarySnapshot> page = dictionaryManagementService.findDictionaries(dictionaryFilter, pageable, identity);
    log.debug("Found dictionaries {number: {}}", page.getTotalElements());
    return ok(page);
  }

  @Override
  public ResponseEntity<DictionaryDetails> getDictionary(UUID dictionaryId) {
    log.trace("Getting dictionary {dictionaryId: {}}", dictionaryId);
    AccountIdentity identity = getIdentityOrThrow();
    DictionaryDetails details = dictionaryManagementService.getDictionary(dictionaryId, identity);
    log.debug("Got dictionary {dictionaryId: {}}", details.getId());
    return ok(details);
  }

  @Override
  public ResponseEntity<DictionaryDetails> updateDictionary(
      UUID dictionaryId, @Valid DictionaryUpdateRequest dictionaryUpdateRequest) {
    log.trace(
        "Updating dictionary {dictionaryId: {}, dictionaryUpdateRequest: {}}",
        dictionaryId,
        dictionaryUpdateRequest);
    AccountIdentity identity = getIdentityOrThrow();
    DictionaryDetails details =
        dictionaryManagementService.updateDictionary(
            dictionaryId, dictionaryUpdateRequest, identity);
    log.debug("Updated dictionary {dictionaryId: {}}", dictionaryId);
    return ok(details);
  }
}
