package pl.com.bit.dictionary.controller.internal;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import pl.com.bit.dictionary.service.internal.DictionaryInternalService;
import pl.com.rszewczyk.stack.dictionary.api.InternalDictionariesApi;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryList;

@Slf4j
@RestController
@RequiredArgsConstructor
public class DictionaryInternalController implements InternalDictionariesApi {
    private final DictionaryInternalService dictionaryInternalService;

    @Override
    public ResponseEntity<DictionaryEntryList> getInternalDictionariesEntries(@Valid DictionaryEntryFilter query) {
        log.trace("Getting dictionary entries {filter: {}}", query);
        DictionaryEntryList list = dictionaryInternalService.findDictionaryEntries(query);
        log.debug("Got dictionary entries {size: {}}", list.getEntryList().size());
        return ResponseEntity.ok(list);
    }
}
