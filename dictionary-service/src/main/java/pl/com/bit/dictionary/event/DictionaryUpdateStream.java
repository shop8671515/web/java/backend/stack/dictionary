package pl.com.bit.dictionary.event;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface DictionaryUpdateStream {
    String NAME = "dictionary-updated-stream";

    interface Sink {
        String INPUT = NAME + "-input";

        @Input(INPUT)
        SubscribableChannel input();
    }

    interface Source {
        String OUTPUT = NAME + "-output";

        @Output(OUTPUT)
        MessageChannel output();
    }
}
