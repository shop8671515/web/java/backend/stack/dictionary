package pl.com.bit.dictionary.exception;

import javax.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class DictionaryAlreadyExistsException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public DictionaryAlreadyExistsException(@NotBlank String dictionaryName) {
    super(String.format("Dictionary with given name %s already exists", dictionaryName));
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.DICTIONARY_ALREADY_EXISTS.name();
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.BAD_REQUEST;
  }
}
