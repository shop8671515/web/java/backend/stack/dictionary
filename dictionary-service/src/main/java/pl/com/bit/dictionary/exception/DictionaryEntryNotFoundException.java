package pl.com.bit.dictionary.exception;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class DictionaryEntryNotFoundException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public DictionaryEntryNotFoundException(@NotNull UUID dictionaryEntryId) {
    super(String.format("Dictionary entry with given id %s not found", dictionaryEntryId));
  }

  public DictionaryEntryNotFoundException(@NotBlank String message) {
    super(message);
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.DICTIONARY_ENTRY_NOT_FOUND.name();
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.NOT_FOUND;
  }
}
