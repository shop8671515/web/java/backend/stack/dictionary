package pl.com.bit.dictionary.exception;

import javax.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class DictionaryNotFoundException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public DictionaryNotFoundException(@NotBlank String message) {
    super(message);
  }

  public DictionaryNotFoundException() {
    super("Dictionary not found");
  }

  public static DictionaryNotFoundException of(@NotBlank String dictionaryName) {
    return new DictionaryNotFoundException(formatMessage(dictionaryName));
  }

  private static String formatMessage(@NotBlank String dictionaryName) {
    return String.format("Dictionary with name of %s not found", dictionaryName);
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.DICTIONARY_NOT_FOUND.name();
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.NOT_FOUND;
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }
}
