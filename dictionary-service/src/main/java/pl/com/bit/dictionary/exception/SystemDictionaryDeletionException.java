package pl.com.bit.dictionary.exception;

import javax.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class SystemDictionaryDeletionException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public SystemDictionaryDeletionException(@NotBlank String dictionaryName) {
    super(String.format("Attempting to delete system dictionary with name %s", dictionaryName));
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.BAD_REQUEST;
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.SYSTEM_DICTIONARY_ATTEMPT_DELETION.name();
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }
}
