package pl.com.bit.dictionary.exception;

import javax.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class SystemDictionaryEntryDeletionException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public SystemDictionaryEntryDeletionException(
      @NotBlank String dictionaryName, @NotBlank String dictionaryKey) {
    super(format(dictionaryName, dictionaryKey));
  }

  public SystemDictionaryEntryDeletionException(@NotBlank String message) {
    super(message);
  }

  private static String format(@NotBlank String dictionaryName, @NotBlank String dictionaryKey) {
    return String.format(
        "Dictionary entry %s is in system dictionary %s", dictionaryKey, dictionaryName);
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.SYSTEM_DICTIONARY_ENTRY_DELETION_ATTEMPT.name();
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.BAD_REQUEST;
  }
}
