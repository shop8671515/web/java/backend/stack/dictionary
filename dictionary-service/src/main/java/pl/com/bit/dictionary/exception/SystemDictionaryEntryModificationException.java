package pl.com.bit.dictionary.exception;

import javax.validation.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode;

public class SystemDictionaryEntryModificationException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public SystemDictionaryEntryModificationException(
      @NotBlank String dictionaryName, @NotBlank String dictionaryKey) {
    super(formatMessage(dictionaryName, dictionaryKey));
  }

  public SystemDictionaryEntryModificationException(@NotBlank String message) {
    super(message);
  }

  private static String formatMessage(
      @NotBlank String dictionaryName, @NotBlank String dictionaryKey) {
    return String.format(
        "Dictionary entry with key of %s is in a system dictionary with name %s",
        dictionaryKey, dictionaryName);
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.SYSTEM_DICTIONARY_ENTRY_ATTEMPT_UPDATE.name();
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.BAD_REQUEST;
  }
}
