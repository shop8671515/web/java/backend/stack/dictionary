package pl.com.bit.dictionary.exception;

import javax.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import pl.com.bit.common.ErrorCode;
import pl.com.bit.common.exception.ErrorCodeAware;
import pl.com.bit.http.problem.ProblemLogLevel;
import pl.com.bit.http.problem.ProblemMetadataAware;

public class VersionMismatchException extends RuntimeException
    implements ErrorCodeAware, ProblemMetadataAware {

  public VersionMismatchException() {
    super("Version mismatch");
  }

  public VersionMismatchException(@NotNull Integer dtoVersion, @NotNull Integer entityVersion) {
    super(
        String.format(
            "Version mismatch: dto version - %d, entity version - %d", dtoVersion, entityVersion));
  }

  @Override
  public HttpStatus getHttpStatus() {
    return HttpStatus.BAD_REQUEST;
  }

  @Override
  public ProblemLogLevel getLogLevel() {
    return ProblemLogLevel.WARN;
  }

  @Override
  public String getErrorCode() {
    return ErrorCode.VERSION_MISMATCH.name();
  }
}
