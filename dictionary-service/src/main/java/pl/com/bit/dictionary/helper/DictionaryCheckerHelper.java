package pl.com.bit.dictionary.helper;

import static lombok.AccessLevel.PRIVATE;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.NoArgsConstructor;
import pl.com.bit.dictionary.common.ComparatorUtils;
import pl.com.bit.dictionary.exception.DictionaryNotFoundException;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.bit.dictionary.model.DictionaryEntry;

@NoArgsConstructor(access = PRIVATE)
public class DictionaryCheckerHelper {

  public static void assertNonSystemDictionary(
      @NotNull @Valid DictionaryEntry dictionaryEntry, @NotNull RuntimeException exception) {
    assertNonSystemDictionary(dictionaryEntry.getDictionary(), exception);
  }

  public static void assertNonSystemDictionary(
      @NotNull @Valid Dictionary dictionary, @NotNull RuntimeException exception) {
    ComparatorUtils.equalsOrThrow(dictionary.isSystem(), false, () -> exception);
  }

  public static void assertDictionaryNotDeleted(@NotNull @Valid Dictionary dictionary) {
    ComparatorUtils.equalsOrThrow(
        dictionary.isDeleted(), false, () -> DictionaryNotFoundException.of(dictionary.getName()));
  }
}
