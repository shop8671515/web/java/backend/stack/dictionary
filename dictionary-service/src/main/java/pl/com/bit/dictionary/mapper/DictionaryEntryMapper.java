package pl.com.bit.dictionary.mapper;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import pl.com.bit.dictionary.model.DictionaryEntry;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface DictionaryEntryMapper {
    DictionaryEntryMapper DICTIONARY_ENTRY_MAPPER = Mappers.getMapper(DictionaryEntryMapper.class);

    DictionaryEntryDetails buildDictionaryEntryDetails(@NotNull @Valid DictionaryEntry dictionaryEntry);

    DictionaryEntryLite buildDictionaryEntryLite(@NotNull @Valid DictionaryEntry dictionaryEntry);

    default Page<DictionaryEntryLite> buildDictionaryEntryLitePage(@NotNull Page<@Valid DictionaryEntry> dictionaryEntries) {
        return dictionaryEntries.map(this::buildDictionaryEntryLite);
    }

    default List<DictionaryEntryLite> buildDictionaryEntryLiteList(@NotNull Page<@Valid DictionaryEntry> dictionaryEntries) {
        return dictionaryEntries.map(this::buildDictionaryEntryLite).toList();
    }
}
