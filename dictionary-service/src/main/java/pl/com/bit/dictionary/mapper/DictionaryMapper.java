package pl.com.bit.dictionary.mapper;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionarySnapshot;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface DictionaryMapper {
  DictionaryMapper DICTIONARY_MAPPER = Mappers.getMapper(DictionaryMapper.class);

  DictionaryDetails buildDictionaryDetails(Dictionary dictionary);

  DictionarySnapshot dictionarySnapshot(@NotNull @Valid Dictionary dictionary);

  default Page<DictionarySnapshot> dictionarySnapshotPage(@NotNull Page<Dictionary> dictionaries) {
    return dictionaries.map(this::dictionarySnapshot);
  }
}
