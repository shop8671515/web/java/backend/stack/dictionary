package pl.com.bit.dictionary.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.javers.core.metamodel.annotation.DiffIgnore;
import pl.com.bit.common.named.object.entity.NamedObjectSnap;
import pl.com.bit.common.versioned.entity.VersionedEntity;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryUpdateRequest;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@NamedEntityGraphs({
  @NamedEntityGraph(
      name = Dictionary.DICTIONARY_TEST_GRAPH,
      attributeNodes = {@NamedAttributeNode("categories")}),
  @NamedEntityGraph(
      name = Dictionary.DICTIONARY_CATEGORY_GRAPH,
      attributeNodes = {
        @NamedAttributeNode("author"),
        @NamedAttributeNode("modifier"),
        @NamedAttributeNode("categories")
      }),
  @NamedEntityGraph(
      name = Dictionary.DICTIONARY_DETAILS_GRAPH,
      attributeNodes = {
        @NamedAttributeNode("author"),
        @NamedAttributeNode("modifier"),
        @NamedAttributeNode("categories"),
        @NamedAttributeNode("entries")
      })
})
public class Dictionary extends VersionedEntity {
  public static final String DICTIONARY_TEST_GRAPH = "dictionary-test-graph";
  public static final String DICTIONARY_CATEGORY_GRAPH = "dictionary-category-graph";
  public static final String DICTIONARY_DETAILS_GRAPH = "dictionary-details-graph";

  @NotBlank private String name;

  @NotBlank private String description;

  private boolean deleted;

  private boolean system;

  @NotNull
  @ToString.Exclude
  @ElementCollection(targetClass = String.class)
  @CollectionTable(name = "dictionary_category", joinColumns = @JoinColumn(name = "dictionary_id"))
  @Column(name = "category", updatable = false)
  private Set<String> categories;

  @NotNull private Boolean quicklyChangeable = false;

  @NotNull
  @DiffIgnore
  @Builder.Default
  @ToString.Exclude
  @OneToMany(
      mappedBy = "dictionary",
      cascade = CascadeType.ALL,
      fetch = FetchType.LAZY,
      orphanRemoval = true)
  private Set<DictionaryEntry> entries = new HashSet<>();

  public static Dictionary of(
      @NotNull NamedObjectSnap author, @NotNull @Valid DictionaryCreateRequest dto) {
    return builder()
        .author(author)
        .authorId(author.getCid())
        .name(dto.getName())
        .description(dto.getDescription())
        .deleted(false)
        .categories(dto.getCategories())
        .quicklyChangeable(false)
        .build();
  }

  public void update(@NotNull NamedObjectSnap author, @NotNull @Valid DictionaryUpdateRequest dto) {
    setName(dto.getName());
    setDescription(dto.getDescription());
    if (Objects.nonNull(dto.getCategories())) {
      getCategories().addAll(dto.getCategories());
    }
    setQuicklyChangeable(dto.isQuicklyChangeable());
    setModifier(author);
  }
}
