package pl.com.bit.dictionary.model;

import com.neovisionaries.i18n.LanguageCode;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import pl.com.bit.common.named.object.entity.NamedObjectSnap;
import pl.com.bit.common.versioned.entity.VersionedEntity;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryUpdateRequest;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@NamedEntityGraphs({
  @NamedEntityGraph(
      name = DictionaryEntry.DICTIONARY_ENTRY_LITE_GRAPH,
      attributeNodes = {@NamedAttributeNode("id")}),
  @NamedEntityGraph(
      name = DictionaryEntry.DICTIONARY_ENTRY_DETAILS_GRAPH,
      attributeNodes = {
        @NamedAttributeNode("author"),
        @NamedAttributeNode("modifier"),
        @NamedAttributeNode("dictionary")
      })
})
public class DictionaryEntry extends VersionedEntity {
  public static final String DICTIONARY_ENTRY_LITE_GRAPH = "dictionary-entry-lite-graph";
  public static final String DICTIONARY_ENTRY_DETAILS_GRAPH = "dictionary-entry-details-graph";

  @NotNull
  @Enumerated(EnumType.STRING)
  private LanguageCode language;

  @NotBlank private String key;

  @NotBlank private String value;

  private boolean deleted;

  @NotNull
  @ToString.Exclude
  @EqualsAndHashCode.Exclude
  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "dictionary_id")
  private Dictionary dictionary;

  public static DictionaryEntry of(
      @NotNull @Valid DictionaryEntryCreateRequest dto,
      @NotNull @Valid Dictionary dictionary,
      @NotNull NamedObjectSnap author) {
    return builder()
        .author(author)
        .authorId(author.getCid())
        .dictionary(dictionary)
        .language(LanguageCode.getByCode(dto.getLanguage()))
        .key(dto.getKey())
        .value(dto.getValue())
        .build();
  }

  public void update(
      @NotNull @Valid DictionaryEntryUpdateRequest dto, @NotNull NamedObjectSnap modifier) {
    setLanguage(LanguageCode.getByCode(dto.getLanguage()));
    setKey(dto.getKey());
    setValue(dto.getValue());
    setModifier(modifier);
  }
}
