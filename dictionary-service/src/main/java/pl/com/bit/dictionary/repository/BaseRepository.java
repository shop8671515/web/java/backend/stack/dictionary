package pl.com.bit.dictionary.repository;

import javax.validation.constraints.NotNull;
import pl.com.bit.dictionary.common.ComparatorUtils;
import pl.com.bit.dictionary.exception.VersionMismatchException;

public interface BaseRepository {

  default void checkVersion(@NotNull Integer dtoVersion, @NotNull Integer entityVersion) {
    ComparatorUtils.equalsOrThrow(
        dtoVersion, entityVersion, () -> new VersionMismatchException(dtoVersion, entityVersion));
  }
}
