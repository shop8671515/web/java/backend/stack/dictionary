package pl.com.bit.dictionary.repository;

import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import pl.com.bit.common.named.object.entity.NamedObjectSnap;
import pl.com.bit.dictionary.exception.DictionaryEntryNotFoundException;
import pl.com.bit.dictionary.model.DictionaryEntry;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;

@Repository
public interface DictionaryEntryRepository
    extends BaseRepository,
        JpaRepository<DictionaryEntry, UUID>,
        QuerydslPredicateExecutor<DictionaryEntry>,
        DictionaryEntryRepositoryCustom {

  default DictionaryEntry getDictionaryEntryById(@NotNull UUID dictionaryEntryId) {
    return getDictionaryEntryById(
        dictionaryEntryId, DictionaryEntry.DICTIONARY_ENTRY_DETAILS_GRAPH);
  }

  default DictionaryEntry getDictionaryEntryById(
      @NotNull UUID dictionaryEntryId, @NotBlank String graphName) {
    return findDictionaryEntryById(dictionaryEntryId, graphName)
        .orElseThrow(() -> new DictionaryEntryNotFoundException(dictionaryEntryId));
  }

  default DictionaryEntry getDeletedDictionaryEntryById(@NotNull UUID dictionaryEntryId) {
    return findDeletedDictionaryEntryById(dictionaryEntryId)
        .orElseThrow(() -> new DictionaryEntryNotFoundException(dictionaryEntryId));
  }

  default Page<DictionaryEntry> find(
      @NotNull DictionaryEntryFilter filter, @NotNull Pageable pageable) {
    return findAllOptimized(filter, pageable);
  }

  @Modifying
  @Query("UPDATE DictionaryEntry de SET de.deleted = TRUE, de.modifier = ?2 WHERE de.id = ?1")
  int softDeleteById(@NotNull UUID dictionaryEntryId, @NotNull NamedObjectSnap modifier);
}
