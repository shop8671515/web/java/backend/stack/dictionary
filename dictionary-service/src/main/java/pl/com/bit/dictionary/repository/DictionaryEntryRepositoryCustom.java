package pl.com.bit.dictionary.repository;

import com.querydsl.core.types.Predicate;
import java.util.Optional;
import java.util.UUID;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.com.bit.dictionary.model.DictionaryEntry;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;

public interface DictionaryEntryRepositoryCustom {

  Optional<DictionaryEntry> findDictionaryEntryById(
      @NotNull UUID dictionaryEntryId, @NotBlank String graphName);

  Optional<DictionaryEntry> findDeletedDictionaryEntryById(@NotNull UUID dictionaryEntryId);

  Predicate buildPredicate(@NotNull DictionaryEntryFilter filter);

  Page<DictionaryEntry> findAllOptimized(
      @NotNull DictionaryEntryFilter filter, @NotNull Pageable pageable);
}
