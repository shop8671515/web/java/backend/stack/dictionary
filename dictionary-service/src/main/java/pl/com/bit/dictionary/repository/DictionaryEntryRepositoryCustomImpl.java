package pl.com.bit.dictionary.repository;

import static pl.com.bit.dictionary.model.QDictionaryEntry.dictionaryEntry;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.validation.constraints.NotNull;
import org.hibernate.graph.GraphSemantic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import pl.com.bit.common.querydsl.Predicates;
import pl.com.bit.dictionary.model.DictionaryEntry;
import pl.com.bit.dictionary.model.QDictionaryEntry;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;

@Repository
public class DictionaryEntryRepositoryCustomImpl extends QuerydslRepositorySupportWrapper
    implements DictionaryEntryRepositoryCustom {
  private final JPAQueryFactory queryFactory;
  private final EntityManager entityManager;

  public DictionaryEntryRepositoryCustomImpl(EntityManager entityManager) {
    super(QDictionaryEntry.class);
    this.entityManager = entityManager;
    this.queryFactory = new JPAQueryFactory(entityManager);
  }

  @Override
  public Optional<DictionaryEntry> findDictionaryEntryById(
      UUID dictionaryEntryId, String graphName) {
    return Optional.ofNullable(
        queryFactory
            .select(dictionaryEntry)
            .from(dictionaryEntry)
            .where(dictionaryEntry.id.eq(dictionaryEntryId).and(dictionaryEntry.deleted.isFalse()))
            .setHint(GraphSemantic.FETCH.getJpaHintName(), entityManager.getEntityGraph(graphName))
            .fetchOne());
  }

  @Override
  public Optional<DictionaryEntry> findDeletedDictionaryEntryById(UUID dictionaryEntryId) {
    return Optional.ofNullable(
        queryFactory
            .select(dictionaryEntry)
            .from(dictionaryEntry)
            .where(dictionaryEntry.id.eq(dictionaryEntryId).and(dictionaryEntry.deleted.isTrue()))
            .fetchOne());
  }

  @Override
  public Predicate buildPredicate(@NotNull DictionaryEntryFilter filter) {
    return Predicates.builder()
        .eq(dictionaryEntry.dictionary.id, filter.getDictionaryId())
        .eq(dictionaryEntry.dictionary.name, filter.getDictionaryName())
        .build();
  }

  @Override
  public Page<DictionaryEntry> findAllOptimized(DictionaryEntryFilter filter, Pageable pageable) {
    JPQLQuery<UUID> query =
        from(dictionaryEntry).select(dictionaryEntry.id).where(buildPredicate(filter));
    JPQLQuery<UUID> pagedQuery = getNotNullQuerydsl().applyPagination(pageable, query);
    List<UUID> ids = pagedQuery.fetch();

    if (CollectionUtils.isEmpty(ids)) {
      return Page.empty(pageable);
    }
    List<DictionaryEntry> entries =
        queryFactory
            .select(dictionaryEntry)
            .from(dictionaryEntry)
            .where(dictionaryEntry.id.in(ids))
            .fetch();
    entries.sort(Comparator.comparing(dictionaryEntry -> ids.indexOf(dictionaryEntry.getId())));
    return new PageImpl<>(entries, pageable, query.fetchCount());
  }
}
