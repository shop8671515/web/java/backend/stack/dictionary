package pl.com.bit.dictionary.repository;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import pl.com.bit.common.named.object.entity.NamedObjectSnap;
import pl.com.bit.dictionary.exception.DictionaryNotFoundException;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter;

@Repository
public interface DictionaryRepository
    extends BaseRepository,
        JpaRepository<Dictionary, UUID>,
        QuerydslPredicateExecutor<Dictionary>,
        DictionaryRepositoryCustom {

  default Dictionary getDictionaryById(@NotNull UUID dictionaryId, @NotBlank String graphName) {
    return findDictionaryById(dictionaryId, graphName)
        .orElseThrow(
            () ->
                new DictionaryNotFoundException(
                    String.format("Dictionary with given id: %s not found", dictionaryId)));
  }

  default Dictionary getDictionaryById(@NotNull UUID dictionaryId) {
    return getDictionaryById(dictionaryId, Dictionary.DICTIONARY_DETAILS_GRAPH);
  }

  default Dictionary getDeletedById(@NotNull UUID dictionaryId) {
    return findDeletedDictionaryById(dictionaryId)
        .orElseThrow(
            () ->
                new DictionaryNotFoundException(
                    String.format("Dictionary with given id: %s not found", dictionaryId)));
  }

  default Dictionary getDictionaryByEntryId(@NotNull UUID dictionaryEntryId) {
    return findDictionaryByEntryId(dictionaryEntryId).orElseThrow(DictionaryNotFoundException::new);
  }

  default Page<Dictionary> find(
      @NotNull @Valid DictionaryFilter filter, @NotNull Pageable pageable) {
    return findAllOptimized(filter, pageable);
  }

  @Modifying
  @Query("UPDATE Dictionary d SET d.deleted = TRUE, d.modifier = ?2 WHERE d.id = ?1")
  int softDeleteById(@NotNull UUID id, @NotNull NamedObjectSnap modifier);

  default Dictionary getDictionaryByName(@NotBlank String name) {
    return findDictionaryByName(name).orElse(null);
  }
}
