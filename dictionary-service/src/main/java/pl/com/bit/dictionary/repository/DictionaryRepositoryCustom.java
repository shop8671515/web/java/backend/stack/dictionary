package pl.com.bit.dictionary.repository;

import com.querydsl.core.types.Predicate;
import java.util.Optional;
import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter;

public interface DictionaryRepositoryCustom {

  Optional<Dictionary> findDictionaryById(@NotNull UUID dictionaryId, @NotBlank String graphName);

  Optional<Dictionary> findDictionaryByName(@NotBlank String dictionaryName);

  Optional<Dictionary> findDeletedDictionaryById(@NotNull UUID dictionaryId);

  Optional<Dictionary> findDictionaryByEntryId(@NotNull UUID dictionaryEntryId);

  Predicate buildPredicate(@NotNull @Valid DictionaryFilter dictionaryFilter);

  Page<Dictionary> findAllOptimized(
      @NotNull @Valid DictionaryFilter filter, @NotNull Pageable pageable);
}
