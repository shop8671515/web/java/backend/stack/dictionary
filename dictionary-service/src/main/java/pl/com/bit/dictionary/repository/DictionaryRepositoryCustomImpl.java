package pl.com.bit.dictionary.repository;

import static pl.com.bit.dictionary.model.QDictionary.dictionary;

import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import org.hibernate.graph.GraphSemantic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import pl.com.bit.common.querydsl.Predicates;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.bit.dictionary.model.QDictionary;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter;

@Repository
public class DictionaryRepositoryCustomImpl extends QuerydslRepositorySupportWrapper
    implements DictionaryRepositoryCustom {
  private final JPAQueryFactory queryFactory;
  private final EntityManager entityManager;

  public DictionaryRepositoryCustomImpl(EntityManager entityManager) {
    super(QDictionary.class);
    this.entityManager = entityManager;
    this.queryFactory = new JPAQueryFactory(entityManager);
  }

  @Override
  public Optional<Dictionary> findDictionaryById(
      @NotNull UUID dictionaryId, @NotBlank String graphName) {
    return Optional.ofNullable(
        queryFactory
            .select(dictionary)
            .from(dictionary)
            .where(dictionary.id.eq(dictionaryId).and(dictionary.deleted.isFalse()))
            .setHint(GraphSemantic.FETCH.getJpaHintName(), entityManager.getEntityGraph(graphName))
            .fetchOne());
  }

  @Override
  public Optional<Dictionary> findDictionaryByName(@NotBlank String dictionaryName) {
    return Optional.ofNullable(
        queryFactory
            .select(dictionary)
            .from(dictionary)
            .where(dictionary.name.eq(dictionaryName).and(dictionary.deleted.isFalse()))
            .setHint(
                GraphSemantic.FETCH.getJpaHintName(),
                entityManager.getEntityGraph(Dictionary.DICTIONARY_CATEGORY_GRAPH))
            .fetchOne());
  }

  @Override
  public Optional<Dictionary> findDeletedDictionaryById(@NotNull UUID dictionaryId) {
    return Optional.ofNullable(
        queryFactory
            .select(dictionary)
            .from(dictionary)
            .where(dictionary.id.eq(dictionaryId).and(dictionary.deleted.isTrue()))
            .fetchOne());
  }

  @Override
  public Optional<Dictionary> findDictionaryByEntryId(@NotNull UUID dictionaryEntryId) {
    return Optional.ofNullable(
        queryFactory
            .select(dictionary)
            .from(dictionary)
            .where(dictionary.entries.any().id.eq(dictionaryEntryId))
            .fetchOne());
  }

  @Override
  public Predicate buildPredicate(DictionaryFilter dictionaryFilter) {
    return Predicates.builder()
        .in(dictionary.categories, dictionaryFilter.getCategories())
        .eq(dictionary.quicklyChangeable, dictionaryFilter.isQuicklyChangeable())
        .build();
  }

  @Override
  public Page<Dictionary> findAllOptimized(
      @NotNull @Valid DictionaryFilter filter, @NotNull Pageable pageable) {
    JPQLQuery<UUID> query = from(dictionary).select(dictionary.id).where(buildPredicate(filter));
    JPQLQuery<UUID> pagedQuery = getNotNullQuerydsl().applyPagination(pageable, query);
    List<UUID> ids = pagedQuery.fetch();

    if (CollectionUtils.isEmpty(ids)) {
      return Page.empty(pageable);
    }
    List<Dictionary> entries =
        queryFactory
            .select(dictionary)
            .from(dictionary)
            .where(dictionary.id.in(ids))
            .setHint(GraphSemantic.FETCH.getJpaHintName(), Dictionary.DICTIONARY_CATEGORY_GRAPH)
            .fetch();
    entries.sort(Comparator.comparing(dictionary -> ids.indexOf(dictionary.getId())));
    return new PageImpl<>(entries, pageable, query.fetchCount());
  }
}
