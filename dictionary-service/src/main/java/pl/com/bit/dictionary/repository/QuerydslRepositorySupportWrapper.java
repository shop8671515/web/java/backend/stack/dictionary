package pl.com.bit.dictionary.repository;

import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.springframework.data.jpa.repository.support.Querydsl;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;

public class QuerydslRepositorySupportWrapper extends QuerydslRepositorySupport {
  public QuerydslRepositorySupportWrapper(@NotNull Class<?> domainClass) {
    super(domainClass);
  }

  public Querydsl getNotNullQuerydsl() {
    if (Objects.isNull(this.getQuerydsl())) {
      throw new IllegalStateException("Querydsl is null!");
    } else {
      return getQuerydsl();
    }
  }
}
