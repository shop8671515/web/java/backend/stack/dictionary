package pl.com.bit.dictionary.service.external;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.com.bit.common.security.api.AccountIdentity;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryUpdateRequest;

public interface DictionaryEntryManagementService {

  DictionaryEntryDetails createDictionaryEntry(
      @NotNull @Valid DictionaryEntryCreateRequest request, @NotNull AccountIdentity identity);

  void deleteDictionaryEntry(@NotNull UUID dictionaryEntryId, @NotNull AccountIdentity identity);

  DictionaryEntryDetails getDictionaryEntry(
      @NotNull UUID dictionaryEntryId, @NotNull AccountIdentity identity);

  DictionaryEntryDetails updateDictionaryEntry(
      @NotNull UUID dictionaryEntryId,
      @NotNull @Valid DictionaryEntryUpdateRequest request,
      @NotNull AccountIdentity identity);

  Page<DictionaryEntryLite> getDictionaryEntries(
      @NotNull DictionaryEntryFilter filter,
      @NotNull Pageable pageable,
      @NotNull AccountIdentity identity);
}
