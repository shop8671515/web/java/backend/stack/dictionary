package pl.com.bit.dictionary.service.external;

import static pl.com.bit.dictionary.helper.DictionaryCheckerHelper.assertDictionaryNotDeleted;
import static pl.com.bit.dictionary.helper.DictionaryCheckerHelper.assertNonSystemDictionary;
import static pl.com.bit.dictionary.mapper.DictionaryEntryMapper.DICTIONARY_ENTRY_MAPPER;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.com.bit.common.named.object.entity.NamedObjectSnap;
import pl.com.bit.common.security.api.AccountIdentity;
import pl.com.bit.common.security.api.context.WithUserContext;
import pl.com.bit.dictionary.exception.SystemDictionaryEntryDeletionException;
import pl.com.bit.dictionary.exception.SystemDictionaryEntryModificationException;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.bit.dictionary.model.DictionaryEntry;
import pl.com.bit.dictionary.repository.DictionaryEntryRepository;
import pl.com.bit.dictionary.repository.DictionaryRepository;
import pl.com.bit.user.domain.user.api.snap.UserSnapService;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryUpdateRequest;

@Slf4j
@Service
@WithUserContext
@RequiredArgsConstructor
public class DictionaryEntryManagementServiceImpl implements DictionaryEntryManagementService {
  private final DictionaryEntryRepository dictionaryEntryRepository;
  private final DictionaryRepository dictionaryRepository;
  private final UserSnapService userSnapService;

  @Override
  @Transactional
  public DictionaryEntryDetails createDictionaryEntry(
      @NotNull @Valid DictionaryEntryCreateRequest request, @NotNull AccountIdentity identity) {
    log.trace("Creating dictionary entry {request: {}}", request);
    NamedObjectSnap author = userSnapService.get(identity.getId());
    Dictionary dictionary = dictionaryRepository.getDictionaryById(request.getDictionaryId());
    DictionaryEntry dictionaryEntry = DictionaryEntry.of(request, dictionary, author);
    dictionaryEntry = dictionaryEntryRepository.saveAndFlush(dictionaryEntry);
    log.debug("Created dictionary entry {entryId: {}}", dictionaryEntry.getId());
    return DICTIONARY_ENTRY_MAPPER.buildDictionaryEntryDetails(dictionaryEntry);
  }

  @Override
  @Transactional
  public void deleteDictionaryEntry(
      @NotNull UUID dictionaryEntryId, @NotNull AccountIdentity identity) {
    log.trace("Deleting dictionary entry {entryId: {}}", dictionaryEntryId);
    NamedObjectSnap modifier = userSnapService.get(identity.getId());
    DictionaryEntry dictionaryEntry =
        dictionaryEntryRepository.getDictionaryEntryById(dictionaryEntryId);
    assertNonSystemDictionary(
        dictionaryEntry,
        new SystemDictionaryEntryDeletionException(
            dictionaryEntry.getDictionary().getName(), dictionaryEntry.getKey()));
    dictionaryEntryRepository.softDeleteById(dictionaryEntryId, modifier);
    log.debug("Deleted dictionary entry");
  }

  @Override
  @Transactional(readOnly = true)
  public DictionaryEntryDetails getDictionaryEntry(
      @NotNull UUID dictionaryEntryId, @NotNull AccountIdentity identity) {
    log.trace("Getting dictionary entry {entryId: {}}", dictionaryEntryId);
    DictionaryEntry dictionaryEntry =
        dictionaryEntryRepository.getDictionaryEntryById(dictionaryEntryId);
    assertDictionaryNotDeleted(dictionaryEntry.getDictionary());
    log.debug("Got dictionary entry {entryId: {}}", dictionaryEntry.getId());
    return DICTIONARY_ENTRY_MAPPER.buildDictionaryEntryDetails(dictionaryEntry);
  }

  @Override
  @Transactional
  public DictionaryEntryDetails updateDictionaryEntry(
      @NotNull UUID dictionaryEntryId,
      @NotNull @Valid DictionaryEntryUpdateRequest request,
      @NotNull AccountIdentity identity) {
    log.trace("Updating dictionary entry {entryId: {}, request: {}}", dictionaryEntryId, request);
    NamedObjectSnap modifier = userSnapService.get(identity.getId());
    Dictionary dictionary = dictionaryRepository.getDictionaryByEntryId(dictionaryEntryId);
    if (dictionary.isSystem()) {
      throw new SystemDictionaryEntryModificationException(
          "Attempting to update system dictionary entry");
    }
    DictionaryEntry dictionaryEntry =
        dictionaryEntryRepository.getDictionaryEntryById(dictionaryEntryId);
    dictionaryEntryRepository.checkVersion(dictionaryEntry.getVersion(), request.getVersion());
    dictionaryEntry.update(request, modifier);
    dictionaryEntry = dictionaryEntryRepository.saveAndFlush(dictionaryEntry);
    log.debug("Updated dictionary entry {entryId: {}}", dictionaryEntry.getId());
    return DICTIONARY_ENTRY_MAPPER.buildDictionaryEntryDetails(dictionaryEntry);
  }

  @Override
  public Page<DictionaryEntryLite> getDictionaryEntries(
      @NotNull DictionaryEntryFilter filter,
      @NotNull Pageable pageable,
      @NotNull AccountIdentity identity) {
    log.trace("Getting all dictionary entries {filter: {}}", filter);
    NamedObjectSnap user = userSnapService.get(identity.getId());
    Page<DictionaryEntry> dictionaryEntries = dictionaryEntryRepository.find(filter, pageable);
    log.debug(
        "Got all dictionary entries by dictionary id {number: {}}",
        dictionaryEntries.getTotalElements());
    return DICTIONARY_ENTRY_MAPPER.buildDictionaryEntryLitePage(dictionaryEntries);
  }
}
