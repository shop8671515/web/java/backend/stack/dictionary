package pl.com.bit.dictionary.service.external;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.com.bit.common.security.api.AccountIdentity;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionarySnapshot;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryUpdateRequest;

public interface DictionaryManagementService {

  DictionaryDetails createDictionary(
      @NotNull @Valid DictionaryCreateRequest request, @NotNull AccountIdentity identity);

  void deleteDictionary(@NotNull UUID dictionaryId, @NotNull AccountIdentity identity);

  DictionaryDetails getDictionary(@NotNull UUID dictionaryId, @NotNull AccountIdentity identity);

  Page<DictionarySnapshot> findDictionaries(
      @NotNull @Valid DictionaryFilter filter,
      @NotNull Pageable pageable,
      @NotNull AccountIdentity identity);

  DictionaryDetails updateDictionary(
      @NotNull UUID dictionaryId,
      @NotNull DictionaryUpdateRequest request,
      @NotNull AccountIdentity identity);
}
