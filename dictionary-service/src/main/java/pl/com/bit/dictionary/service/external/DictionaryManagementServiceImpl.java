package pl.com.bit.dictionary.service.external;

import static pl.com.bit.dictionary.mapper.DictionaryMapper.DICTIONARY_MAPPER;

import java.util.UUID;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.com.bit.common.named.object.entity.NamedObjectSnap;
import pl.com.bit.common.security.api.AccountIdentity;
import pl.com.bit.common.security.api.context.WithUserContext;
import pl.com.bit.dictionary.common.ComparatorUtils;
import pl.com.bit.dictionary.exception.DictionaryAlreadyExistsException;
import pl.com.bit.dictionary.exception.SystemDictionaryDeletionException;
import pl.com.bit.dictionary.helper.DictionaryCheckerHelper;
import pl.com.bit.dictionary.model.Dictionary;
import pl.com.bit.dictionary.repository.DictionaryRepository;
import pl.com.bit.user.domain.user.api.snap.UserSnapService;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryCreateRequest;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryDetails;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionarySnapshot;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryUpdateRequest;

@Slf4j
@Service
@WithUserContext
@RequiredArgsConstructor
public class DictionaryManagementServiceImpl implements DictionaryManagementService {
  private final DictionaryRepository dictionaryRepository;
  private final UserSnapService userSnapService;

  @Override
  @Transactional
  public DictionaryDetails createDictionary(
      DictionaryCreateRequest request, AccountIdentity identity) {
    log.trace("Creating dictionary {request: {}}", request);
    NamedObjectSnap author = userSnapService.get(identity.getId());
    assertDictionaryNotExists(request.getName());
    Dictionary dictionary = Dictionary.of(author, request);
    DictionaryDetails details =
        DICTIONARY_MAPPER.buildDictionaryDetails(dictionaryRepository.saveAndFlush(dictionary));
    log.debug("Created dictionary {dictionaryId: {}}", dictionary.getId());
    return details;
  }

  @Override
  @Transactional
  public void deleteDictionary(UUID dictionaryId, AccountIdentity identity) {
    log.trace("Deleting dictionary {dictionaryId: {}}", dictionaryId);
    Dictionary dictionary = dictionaryRepository.getDictionaryById(dictionaryId);
    DictionaryCheckerHelper.assertNonSystemDictionary(
        dictionary, new SystemDictionaryDeletionException(dictionary.getName()));
    NamedObjectSnap modifier = userSnapService.get(identity.getId());
    dictionaryRepository.softDeleteById(dictionaryId, modifier);
    log.debug("Deleted dictionary");
  }

  @Override
  @Transactional(readOnly = true)
  public DictionaryDetails getDictionary(UUID dictionaryId, AccountIdentity identity) {
    log.trace("Getting dictionary {dictionaryId: {}}", dictionaryId);
    NamedObjectSnap modifier = userSnapService.get(identity.getId());
    Dictionary dictionary = dictionaryRepository.getDictionaryById(dictionaryId);
    DictionaryDetails details = DICTIONARY_MAPPER.buildDictionaryDetails(dictionary);
    log.debug("Got dictionary {dictionaryId: {}}", dictionaryId);
    return details;
  }

  @Override
  @Transactional(readOnly = true)
  public Page<DictionarySnapshot> findDictionaries(
      @NotNull @Valid DictionaryFilter filter,
      @NotNull Pageable pageable,
      @NotNull AccountIdentity identity) {
    log.trace("Finding dictionaries {filter: {}}", filter);
    Page<Dictionary> dictionaries = dictionaryRepository.find(filter, pageable);
    log.debug("Found dictionaries: {number: {}}", dictionaries.getTotalElements());
    return DICTIONARY_MAPPER.dictionarySnapshotPage(dictionaries);
  }

  @Override
  @Transactional
  public DictionaryDetails updateDictionary(
      UUID dictionaryId, DictionaryUpdateRequest request, AccountIdentity identity) {
    log.trace(
        "Updating dictionary {dictionaryId: {}, dictionaryUpdateRequest: {}}",
        dictionaryId,
        request);
    NamedObjectSnap modifier = userSnapService.get(identity.getId());
    Dictionary dictionary = dictionaryRepository.getDictionaryById(dictionaryId);
    dictionaryRepository.checkVersion(dictionary.getVersion(), request.getVersion());
    dictionary.update(modifier, request);
    dictionary = dictionaryRepository.saveAndFlush(dictionary);
    log.debug("Updated dictionary {dictionaryId: {}}", dictionaryId);
    return DICTIONARY_MAPPER.buildDictionaryDetails(dictionary);
  }

  private void assertDictionaryNotExists(@NotBlank String name) {
    ComparatorUtils.equalsOrThrow(
        dictionaryRepository.getDictionaryByName(name),
        null,
        () -> new DictionaryAlreadyExistsException(name));
  }
}
