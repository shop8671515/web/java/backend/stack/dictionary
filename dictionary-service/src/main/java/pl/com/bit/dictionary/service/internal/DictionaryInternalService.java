package pl.com.bit.dictionary.service.internal;

import javax.validation.constraints.NotNull;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryList;

public interface DictionaryInternalService {

  DictionaryEntryList findDictionaryEntries(@NotNull DictionaryEntryFilter filter);
}
