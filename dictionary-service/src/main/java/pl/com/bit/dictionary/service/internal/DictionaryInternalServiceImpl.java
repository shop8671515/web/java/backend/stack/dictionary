package pl.com.bit.dictionary.service.internal;

import static pl.com.bit.dictionary.mapper.DictionaryEntryMapper.DICTIONARY_ENTRY_MAPPER;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.com.bit.dictionary.model.DictionaryEntry;
import pl.com.bit.dictionary.repository.DictionaryEntryRepository;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter;
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryList;

@Slf4j
@Service
@RequiredArgsConstructor
public class DictionaryInternalServiceImpl implements DictionaryInternalService {
  private final DictionaryEntryRepository dictionaryEntryRepository;

  @Override
  public DictionaryEntryList findDictionaryEntries(DictionaryEntryFilter filter) {
    log.trace("Getting dictionary entries {filter: {}}", filter);
    Page<DictionaryEntry> dictionaryEntries =
        dictionaryEntryRepository.find(filter, Pageable.unpaged());
    log.debug("Got dictionary entries {size: {}}", dictionaryEntries.toList().size());
    return new DictionaryEntryList(
        DICTIONARY_ENTRY_MAPPER.buildDictionaryEntryLiteList(dictionaryEntries));
  }
}
