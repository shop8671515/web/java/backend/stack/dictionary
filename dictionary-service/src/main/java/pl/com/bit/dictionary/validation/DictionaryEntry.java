package pl.com.bit.dictionary.validation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target({FIELD, PARAMETER, TYPE_USE})
@Constraint(validatedBy = DictionaryEntryValidator.class)
public @interface DictionaryEntry {

  String message() default DictionaryViolationMessageUtils.SINGLE_KEY_MESSAGE;

  Class<?>[] groups() default {};

  Class<? extends Payload>[] payload() default {};

  String[] value() default {};
}
