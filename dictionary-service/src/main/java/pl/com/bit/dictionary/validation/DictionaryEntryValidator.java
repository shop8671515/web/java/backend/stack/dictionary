package pl.com.bit.dictionary.validation;

import static java.util.Objects.isNull;

import java.util.Optional;
import java.util.stream.Stream;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraints.NotBlank;

public class DictionaryEntryValidator implements ConstraintValidator<DictionaryEntry, String> {
  private static Service service;
  private String[] dictionaryNames;

  @Override
  public boolean isValid(String key, ConstraintValidatorContext constraintValidatorContext) {
    return isNull(key)
        || Optional.ofNullable(service)
            .map(s -> Stream.of(dictionaryNames).anyMatch(d -> s.translationExists(d, key)))
            .orElse(Boolean.TRUE);
  }

  public interface Service {
    boolean translationExists(@NotBlank String dictionaryName, @NotBlank String entry);
  }
}
