package pl.com.bit.dictionary.validation;

import java.util.regex.Pattern;
import lombok.experimental.UtilityClass;
import org.apache.commons.text.StringSubstitutor;
import org.apache.groovy.util.Maps;

@UtilityClass
public class DictionaryViolationMessageUtils {

  static final String SINGLE_KEY_MESSAGE =
      "No translation entry with key '${validatedValue}' exists in dictionaries '${value}'";

  private static final Pattern SINGLE_KEY_PATTERN =
      Pattern.compile(
          StringSubstitutor.replace(
              SINGLE_KEY_MESSAGE, Maps.of("validatedValue", "(.*)", "value", "\\$\\[(.*)\\]")));
}
