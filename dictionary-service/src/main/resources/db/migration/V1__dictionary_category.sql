CREATE TABLE dictionary_category
(
    dictionary_id UUID NOT NULL,
    category      TEXT NOT NULL
)