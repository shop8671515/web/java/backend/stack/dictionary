CREATE TABLE dictionary (
    id                  UUID PRIMARY KEY            NOT NULL,
    author_id           UUID                        NOT NULL,
    author_type         TEXT DEFAULT 'USER'::TEXT   NOT NULL,
    created             TIMESTAMP WITH TIME ZONE    NOT NULL,
    modifier_id         UUID,
    modifier_type       TEXT DEFAULT 'USER'::TEXT,
    modified            TIMESTAMP WITH TIME ZONE,
    name                TEXT                        NOT NULL,
    description         TEXT                        NOT NULL,
    deleted             BOOLEAN                     NOT NULL DEFAULT FALSE,
    quickly_changeable  BOOLEAN                     NOT NULL DEFAULT FALSE,
    version             INT                         NOT NULL
)
