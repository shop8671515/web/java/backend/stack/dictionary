CREATE TABLE dictionary_entry (
    id                  UUID                        NOT NULL PRIMARY KEY,
    language            TEXT                        NOT NULL,
    key                 TEXT                        NOT NULL,
    value               TEXT                        NOT NULL,
    dictionary_id       UUID                        NOT NULL,
    author_id           UUID                        NOT NULL,
    author_type         TEXT DEFAULT 'USER'::TEXT   NOT NULL,
    created             TIMESTAMP WITH TIME ZONE    NOT NULL,
    modifier_id         UUID,
    modifier_type       TEXT DEFAULT 'USER'::TEXT,
    modified            TIMESTAMP WITH TIME ZONE,
    version             INTEGER NOT NULL
);

ALTER TABLE dictionary_entry
    ADD CONSTRAINT "FK_dictionary_entry__dictionary"
        FOREIGN KEY (dictionary_id) REFERENCES dictionary
            ON UPDATE CASCADE ON DELETE CASCADE;
