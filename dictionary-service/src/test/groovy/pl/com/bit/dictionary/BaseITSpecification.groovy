package pl.com.bit.dictionary

import org.spockframework.spring.SpringBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.context.annotation.Import
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.test.context.ActiveProfiles
import pl.com.bit.common.helper.RestSpecIT
import pl.com.bit.common.helper.utils.UriHelper
import pl.com.bit.common.named.object.entity.NamedObjectSnap
import pl.com.bit.common.named.object.repository.NamedObjectSnapRepository
import pl.com.bit.common.versioned.entity.VersionedEntity
import pl.com.bit.dictionary.feign.DictionaryEntriesClient
import pl.com.bit.dictionary.feign.DictionaryEntryManagementsClient
import pl.com.bit.dictionary.feign.DictionaryInternalClient
import pl.com.bit.dictionary.feign.DictionaryManagementsClient
import pl.com.bit.dictionary.model.Dictionary
import pl.com.bit.dictionary.model.DictionaryEntry
import pl.com.bit.dictionary.repository.DictionaryEntryRepository
import pl.com.bit.dictionary.repository.DictionaryRepository
import pl.com.bit.user.domain.user.api.snap.UserSnapService
import pl.com.bit.user.starter.UserInternalService
import pl.com.bit.user.user.api.model.AccountRole
import pl.com.bit.user.user.api.model.AccountStatus
import pl.com.bit.user.user.api.model.UserExtended
import pl.com.bit.user.user.api.model.UserLite
import spock.lang.Shared

import javax.transaction.Transactional
import java.time.LocalDate

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

@Import([UriHelper])
@ActiveProfiles("test")
@EnableFeignClients(clients = [DictionaryEntriesClient, DictionaryInternalClient, DictionaryEntryManagementsClient, DictionaryManagementsClient])
@SpringBootTest(webEnvironment = RANDOM_PORT, classes = [DictionaryServiceApplicationBootstrap])
class BaseITSpecification extends RestSpecIT {

    @LocalServerPort
    int port

    @SpringBean
    UserInternalService userInternalService = Mock()

    @Shared
    Pageable pageable = PageRequest.of(0, 20)

    @Autowired
    UserSnapService userSnapService

    @Autowired
    DictionaryRepository dictionaryRepository

    @Autowired
    DictionaryEntryRepository dictionaryEntryRepository

    @Autowired
    NamedObjectSnapRepository namedObjectSnapRepository

    def setup() {
        ServerPortSetup.localPort = port

        userInternalService.getLite(_ as UUID) >> {
            UUID id -> UserLite.builder()
                    .accountId(id)
                    .name("User ${id}")
                    .login("user-${id}")
                    .build()
        }

        userInternalService.getExtended(_ as UUID) >> {
            UUID accountId -> UserExtended.builder()
                    .accountId(accountId)
                    .registrationDate(LocalDate.now())
                    .email("test@gmail.com")
                    .name("Test${accountId}")
                    .lastName("Test${accountId}")
                    .status(AccountStatus.ACTIVE)
                    .role(AccountRole.CLIENT)
                    .login("test")
                    .build()
        }
    }

    NamedObjectSnap getUserSnap(UUID id = UUID.randomUUID()) {
        userSnapService.get(id)
    }

    @Transactional
    Dictionary saveDictionary(Dictionary dictionary) {
        save(dictionary)
        dictionary = dictionaryRepository.saveAndFlush(dictionary)
        dictionaryRepository.getDictionaryById(dictionary.id, Dictionary.DICTIONARY_TEST_GRAPH)
    }

    @Transactional
    Dictionary saveDeletedDictionary(Dictionary deletedDictionary) {
        save(deletedDictionary)
        deletedDictionary = dictionaryRepository.saveAndFlush(deletedDictionary)
        dictionaryRepository.getDeletedById(deletedDictionary.id)
    }

    @Transactional
    DictionaryEntry saveDictionaryEntry(DictionaryEntry dictionaryEntry) {
        save(dictionaryEntry)
        dictionaryEntry = dictionaryEntryRepository.saveAndFlush(dictionaryEntry)
        dictionaryEntryRepository.getDictionaryEntryById(dictionaryEntry.id)
    }

    @Transactional
    DictionaryEntry saveDeletedDictionaryEntry(DictionaryEntry deletedDictionaryEntry) {
        save(deletedDictionaryEntry)
        deletedDictionaryEntry = dictionaryEntryRepository.saveAndFlush(deletedDictionaryEntry)
        dictionaryEntryRepository.getDeletedDictionaryEntryById(deletedDictionaryEntry.id)
    }

    @Transactional
    <T extends VersionedEntity> void save(T entity) {
        namedObjectSnapRepository.saveAndFlush(entity.author)
        namedObjectSnapRepository.saveAndFlush(entity.modifier)
    }

}
