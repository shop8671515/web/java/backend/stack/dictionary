package pl.com.bit.dictionary

import org.junit.ClassRule
import org.springframework.kafka.test.rule.EmbeddedKafkaRule
import spock.lang.Shared
import spock.lang.Specification

class KafkaSharedSpecification extends Specification {

    @Shared
    @ClassRule
    EmbeddedKafkaRule embeddedKafka = new EmbeddedKafkaRule(1, true)

    def setupSpec() {
        System.setProperty('spring.cloud.stream.kafka.binder.zkNodes', embeddedKafka.embeddedKafka.zookeeperConnectionString)
        System.setProperty('spring.cloud.stream.kafka.binder.defaultZkPort', Integer.toString(embeddedKafka.embeddedKafka.zookeeper.port))
        System.setProperty('spring.kafka.bootstrap-servers', embeddedKafka.embeddedKafka.brokersAsString)
    }
}
