package pl.com.bit.dictionary

import com.netflix.client.config.IClientConfig
import com.netflix.loadbalancer.AbstractServerList
import com.netflix.loadbalancer.Server
import com.netflix.loadbalancer.ServerList
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ServerPortSetup {
    public static int localPort

    @Bean
    ServerList<Server> ribbonServerList() {
        new CustomServerList()
    }

    static class CustomServerList extends AbstractServerList<Server> {

        private IClientConfig clientConfig

        @Override
        List<Server> getInitialListOfServers() {
            return updatedListOfServers
        }

        @Override
        List<Server> getUpdatedListOfServers() {
            [new Server('localhost:' + localPort)]
        }

        @Override
        void initWithNiwsConfig(IClientConfig clientConfig) {
            this.clientConfig = clientConfig
        }
    }
}
