package pl.com.bit.dictionary.feign

import org.springframework.cloud.openfeign.FeignClient
import pl.com.bit.dictionary.feign.config.FeignTestConfiguration
import pl.com.rszewczyk.stack.dictionary.api.DictionaryEntriesApi

@FeignClient(name = "api-dictionary-entries", configuration = FeignTestConfiguration)
interface DictionaryEntriesClient extends DictionaryEntriesApi {
}
