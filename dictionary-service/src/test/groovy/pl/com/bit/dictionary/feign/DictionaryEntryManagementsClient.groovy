package pl.com.bit.dictionary.feign

import org.springframework.cloud.openfeign.FeignClient
import pl.com.bit.dictionary.feign.config.FeignTestConfiguration
import pl.com.rszewczyk.stack.dictionary.api.TranslationManagementsApi

@FeignClient(value = "api-translations-management", configuration = FeignTestConfiguration)
interface DictionaryEntryManagementsClient extends TranslationManagementsApi {

}