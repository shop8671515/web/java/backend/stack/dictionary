package pl.com.bit.dictionary.feign;

import org.springframework.cloud.openfeign.FeignClient
import pl.com.bit.dictionary.feign.config.FeignTestConfiguration
import pl.com.rszewczyk.stack.dictionary.api.InternalDictionariesApi

@FeignClient(name = "api-internal-dictionary", configuration = FeignTestConfiguration)
interface DictionaryInternalClient extends InternalDictionariesApi {
}
