package pl.com.bit.dictionary.feign

import org.springframework.cloud.openfeign.FeignClient
import pl.com.bit.dictionary.feign.config.FeignTestConfiguration
import pl.com.rszewczyk.stack.dictionary.api.DictionariesManagementsApi

@FeignClient(value = "api-dictionaries", configuration = FeignTestConfiguration)
interface DictionaryManagementsClient extends DictionariesManagementsApi { }