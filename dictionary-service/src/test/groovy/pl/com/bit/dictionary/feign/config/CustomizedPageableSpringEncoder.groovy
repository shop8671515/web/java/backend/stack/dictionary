package pl.com.bit.dictionary.feign.config;

import feign.RequestTemplate;
import feign.codec.EncodeException;
import feign.codec.Encoder;
import java.lang.reflect.Type;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;

/**
 * Zapobiega członkowaniu pole,DESC na przecinku <a
 * href="https://github.com/spring-cloud/spring-cloud-openfeign/issues/440">przez feigna</a>.
 */
public class CustomizedPageableSpringEncoder implements Encoder {
    private final Encoder delegate;

    /** Page index parameter name. */
    private static final String PAGE_PARAMETER = "page";

    /** Page size parameter name. */
    private static final String SIZE_PARAMETER = "size";

    /** Sort parameter name. */
    private static final String SORT_PARAMETER = "sort";

    /** Comma separator in URL encoding. */
    private static final String URL_COMMA = "%2C";

    /**
     * Creates a new PageableSpringEncoder with the given delegate for fallback. If no delegate is
     * provided and this encoder cant handle the request, an EncodeException is thrown.
     *
     * @param delegate The optional delegate.
     */
    public CustomizedPageableSpringEncoder(Encoder delegate) {
        this.delegate = delegate;
    }

    @Override
    public void encode(Object object, Type bodyType, RequestTemplate requestTemplate) {
        if (supports(object)) {
            if (object instanceof Pageable) {
                Pageable pageable = (Pageable) object;

                if (pageable.isPaged()) {
                    requestTemplate.query(PAGE_PARAMETER, String.valueOf(pageable.getPageNumber()));
                    requestTemplate.query(SIZE_PARAMETER, String.valueOf(pageable.getPageSize()));
                }

                applySort(requestTemplate, pageable.getSort());
            } else if (object instanceof Sort) {
                Sort sort = (Sort) object;
                applySort(requestTemplate, sort);
            }
        } else {
            if (Objects.nonNull(delegate)) {
                delegate.encode(object, bodyType, requestTemplate);
            } else {
                throw new EncodeException(
                        "PageableSpringEncoder does not support the given object "
                                + object.getClass()
                                + " and no delegate was provided for fallback!");
            }
        }
    }

    private static void applySort(RequestTemplate template, Sort sort) {
        Collection<String> existingSorts = template.queries().get(SORT_PARAMETER);
        List<String> sortQueries =
                Optional.ofNullable(existingSorts).map(ArrayList::new).orElseGet(() -> new ArrayList<>(10));
        sort.iterator()
                .forEachRemaining(
                        order -> sortQueries.add(order.getProperty() + URL_COMMA + order.getDirection()));
        if (!CollectionUtils.isEmpty(sortQueries)) {
            template.query(SORT_PARAMETER, sortQueries);
        }
    }

    protected static boolean supports(Object object) {
        return object instanceof Pageable || object instanceof Sort;
    }
}
