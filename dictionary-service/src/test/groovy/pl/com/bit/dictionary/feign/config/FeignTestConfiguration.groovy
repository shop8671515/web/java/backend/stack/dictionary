package pl.com.bit.dictionary.feign.config

import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.ObjectMapper
import feign.Logger
import feign.RequestInterceptor
import feign.RequestTemplate
import feign.codec.Encoder
import feign.codec.ErrorDecoder
import feign.form.spring.SpringFormEncoder
import org.springframework.beans.factory.ObjectFactory
import org.springframework.boot.autoconfigure.http.HttpMessageConverters
import org.springframework.cloud.openfeign.support.PageJacksonModule
import org.springframework.cloud.openfeign.support.SortJacksonModule
import org.springframework.cloud.openfeign.support.SpringEncoder
import org.springframework.context.annotation.Bean
import pl.com.bit.common.feign.CommonErrorDecoder
import pl.com.bit.common.helper.RestSpecIT

class FeignTestConfiguration {
    static class AccountIdRequestInterceptor implements RequestInterceptor {
        private final String accountId

        AccountIdRequestInterceptor(String accountId) {
            this.accountId = accountId
        }

        @Override
        void apply(RequestTemplate requestTemplate) {
            requestTemplate.header('X-Account-Id', accountId)
        }
    }

    @Bean
    RequestInterceptor accountIdRequestInterceptor() {
        return new AccountIdRequestInterceptor(RestSpecIT.ACCOUNT_ID.toString())
    }

    @Bean
    Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL
    }

    @Bean
    ErrorDecoder commonErrorDecoder(ObjectMapper objectMapper) {
        return new CommonErrorDecoder(objectMapper)
    }

    @Bean
    Encoder pageableEncoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        def encoder = new CustomizedPageableSpringEncoder(springEncoder(messageConverters))
        return encoder
    }


    private static Encoder springEncoder(ObjectFactory<HttpMessageConverters> messageConverters) {
        return new SpringEncoder(new SpringFormEncoder(), messageConverters)
    }
}
