package pl.com.bit.dictionary.helper

import java.time.Duration
import java.time.Instant
import java.time.OffsetDateTime
import java.time.temporal.ChronoUnit

class DateTimeHelper {
    static boolean compare(OffsetDateTime entity, OffsetDateTime dto) {
        Instant entityInstant = entity?.toInstant()
        Instant dtoInstant = dto?.toInstant()

        if (entityInstant && dtoInstant) {
            assert Duration.between(entityInstant, dtoInstant).abs().truncatedTo(ChronoUnit.SECONDS).getSeconds() == 0
        }
        true
    }
}
