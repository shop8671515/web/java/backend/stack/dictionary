package pl.com.bit.dictionary.helper

import com.neovisionaries.i18n.LanguageCode
import pl.com.bit.dictionary.model.DictionaryEntry

import static pl.com.bit.dictionary.helper.VersionedEntityHelper.buildVersionedEntity

class DictionaryEntryHelper {

    static DictionaryEntry defaultDictionaryEntry() {
        DictionaryEntry.builder()
            .copyVersionedDataFrom(buildVersionedEntity())
            .language(LanguageCode.pl)
            .key("test-key")
            .value("test-value")
            .dictionary(null)
            .build()
    }
}
