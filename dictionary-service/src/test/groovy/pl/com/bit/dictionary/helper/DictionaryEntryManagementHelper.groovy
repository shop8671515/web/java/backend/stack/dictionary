package pl.com.bit.dictionary.helper

import pl.com.bit.dictionary.model.Dictionary
import pl.com.bit.dictionary.model.DictionaryEntry
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryCreateRequest
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryDetails
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryUpdateRequest
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryLite

import static pl.com.bit.dictionary.helper.DictionaryManagementHelper.compare
import static pl.com.bit.dictionary.helper.DictionaryManagementHelper.compareLite

class DictionaryEntryManagementHelper {
    static DictionaryEntryCreateRequest defaultDictionaryEntryCreateRequest() {
        new DictionaryEntryCreateRequest(
                dictionaryId: UUID.randomUUID(),
                language: "pl",
                key: "test-key",
                value: "test-value"
        )
    }

    static DictionaryEntryUpdateRequest defaultDictionaryEntryUpdateRequest() {
        new DictionaryEntryUpdateRequest(
                language: "en",
                key: "modified-key",
                value: "modified-value",
                version: 0
        )
    }

    static boolean compare(DictionaryEntry entity, DictionaryEntryCreateRequest request) {
        assert entity.id
        assert entity.author
        assert entity.created
        assert entity.language.toString() == request.language
        assert entity.key == request.key
        assert entity.value == request.value
        true
    }

    static boolean compare(DictionaryEntry entity, DictionaryEntryUpdateRequest request) {
        assert entity.language.toString() == request.language
        assert entity.key == request.key
        assert entity.value == request.value
        assert entity.version == request.version + 1
        true
    }

    static boolean compare(DictionaryEntry entity, DictionaryEntryDetails details) {
        assert entity.id == details.id
        assert entity.language.toString() == details.language
        assert entity.key == details.key
        assert entity.value == details.value
        assert entity.version == details.version
        compareLite(entity.dictionary, details.dictionary)
        true
    }

    static boolean compare(DictionaryEntry entity, DictionaryEntryLite lite) {
        assert entity.id == lite.id
        assert entity.language.toString() == lite.language
        assert entity.key == lite.key
        assert entity.value == lite.value
        true
    }
}
