package pl.com.bit.dictionary.helper

import pl.com.bit.dictionary.model.Dictionary

import static pl.com.bit.dictionary.helper.VersionedEntityHelper.buildVersionedEntity

class DictionaryHelper {

    static Dictionary defaultDictionary() {
        Dictionary.builder()
            .copyVersionedDataFrom(buildVersionedEntity())
            .name("test")
            .description("description")
            .categories(['C1', 'C2'] as Set)
            .deleted(false)
            .quicklyChangeable(false)
            .system(false)
            .build()
    }
}
