package pl.com.bit.dictionary.helper

import pl.com.bit.dictionary.model.Dictionary
import pl.com.bit.dictionary.model.DictionaryEntry
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryCreateRequest
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryDetails
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryLite
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryLite
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryUpdateRequest

class DictionaryManagementHelper {

    static DictionaryCreateRequest defaultDictionaryCreateRequest() {
        new DictionaryCreateRequest(
                name: "test dictionary",
                description: "test description",
                categories: ['c1', 'c2'],
                quicklyChangeable: false
        )
    }

    static DictionaryUpdateRequest defaultDictionaryUpdateRequest() {
        new DictionaryUpdateRequest(
                name: "another dictionary",
                description: "another description",
                categories: ['c3', 'c4'],
                quicklyChangeable: true,
                version: 0
        )
    }

    static boolean compare(Dictionary entity, DictionaryCreateRequest request) {
        assert entity.id
        assert entity.created
        assert entity.author
        assert entity.name == request.name
        assert entity.description == entity.description
        assert entity.quicklyChangeable == entity.quicklyChangeable
        true
    }

    static boolean compare(Dictionary entity, DictionaryDetails details) {
        assert entity.id == details.id
        assert entity.version == details.version
        assert DateTimeHelper.compare(entity.created, details.created)
        assert NamedObjectHelper.compare(entity.author, details.author)
        assert DateTimeHelper.compare(entity.modified, details.modified)
        assert NamedObjectHelper.compare(entity.modifier, details.modifier)
        assert entity.name == details.name
        assert entity.description == details.description
        assert details.categories.containsAll(entity.categories)
        compareEntries(entity.entries, details.entries)
        assert entity.quicklyChangeable == details.quicklyChangeable
        true
    }

    static boolean compareLite(Dictionary entity, DictionaryLite lite) {
        assert entity.name == lite.name
        assert entity.description == lite.description
        true
    }

    static boolean compare(Dictionary entity, DictionaryUpdateRequest request) {
        assert entity.name == request.name
        assert entity.description == request.description
        if (request.categories) {
            entity.categories.containsAll(request.categories)
        }
        assert entity.quicklyChangeable == request.quicklyChangeable
        assert entity.version == request.version + 1
        true
    }

    static boolean compareEntries(Set<DictionaryEntry> entries, List<DictionaryEntryLite> liteList) {
        if (entries && liteList) {
            assert entries.size() == liteList.size()
            [entries.sort { it.id }, liteList.sort { it.id }].transpose().every {
                a, b -> DictionaryEntryManagementHelper.compare((DictionaryEntry) a, (DictionaryEntryLite) b)
            }
        }
        true
    }
}
