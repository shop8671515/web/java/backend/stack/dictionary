package pl.com.bit.dictionary.helper

import pl.com.bit.common.named.object.entity.NamedObjectId
import pl.com.bit.common.named.object.entity.NamedObjectSnap
import pl.com.rszewczyk.stack.dictionary.api.model.NamedObject

class NamedObjectHelper {

    static NamedObjectSnap buildNamedObject(String type = "TYPE", UUID id = UUID.randomUUID(), String name = "name") {
        NamedObjectSnap.builder()
                .cid(new NamedObjectId(id, type))
                .name(name)
                .build()
    }

    static boolean compare(NamedObjectSnap internal, NamedObject external) {
        assert internal?.cid?.id == external?.id
        assert internal?.name == external?.name
        true
    }
}
