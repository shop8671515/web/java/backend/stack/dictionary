package pl.com.bit.dictionary.service.external

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import pl.com.bit.common.exception.CommunicationProblemException
import pl.com.bit.common.exception.ElementNotFoundException
import pl.com.bit.dictionary.BaseITSpecification
import pl.com.bit.dictionary.feign.DictionaryEntryManagementsClient
import pl.com.bit.dictionary.helper.DictionaryEntryHelper
import pl.com.bit.dictionary.helper.DictionaryEntryManagementHelper
import pl.com.bit.dictionary.helper.DictionaryHelper
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode

class DictionaryEntryManagementControllerSpecIT extends BaseITSpecification {

    @Autowired
    DictionaryEntryManagementsClient dictionaryEntryManagementsClient

    def setup() {
        namedObjectSnapRepository.deleteAll()
        dictionaryEntryRepository.deleteAll()
    }

    def "should add translation to a dictionary"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary())
        and:
            def request = DictionaryEntryManagementHelper.defaultDictionaryEntryCreateRequest().tap {
                dictionaryId = dictionary.id
            }
        when:
            def response = dictionaryEntryManagementsClient.createDictionaryEntry(request)
        then:
            response.statusCode == HttpStatus.CREATED
            with(dictionaryEntryRepository.getDictionaryEntryById(response.body.id)) {
                DictionaryEntryManagementHelper.compare(it, request)
                DictionaryEntryManagementHelper.compare(it, response.body)
            }
    }

    def "should not add translation to a deleted dictionary"() {
        given:
            def dictionary = saveDeletedDictionary(DictionaryHelper.defaultDictionary().tap {
                deleted = true
            })
        and:
            def request = DictionaryEntryManagementHelper.defaultDictionaryEntryCreateRequest().tap {
                dictionaryId = dictionary.id
            }
        when:
            dictionaryEntryManagementsClient.createDictionaryEntry(request)
        then:
            def ex = thrown(ElementNotFoundException)
            ex.errorCode == ErrorCode.DICTIONARY_NOT_FOUND.name()
    }

    def "should update dictionary translation"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary())
            def dictionaryEntry = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        and:
            def request = DictionaryEntryManagementHelper.defaultDictionaryEntryUpdateRequest().tap {
                version = dictionaryEntry.version
            }
        when:
            def response = dictionaryEntryManagementsClient.updateDictionaryEntry(dictionaryEntry.id, request)
        then:
            response.statusCode == HttpStatus.OK
            with(dictionaryEntryRepository.getDictionaryEntryById(dictionaryEntry.id)) {
                DictionaryEntryManagementHelper.compare(it, request)
                DictionaryEntryManagementHelper.compare(it, response.body)
            }
    }

    def "should not update system dictionary translation"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary().tap {
                system = true
            })
            def dictionaryEntry = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        and:
            def request = DictionaryEntryManagementHelper.defaultDictionaryEntryUpdateRequest().tap {
                version = dictionaryEntry.version
            }
        when:
            dictionaryEntryManagementsClient.updateDictionaryEntry(dictionaryEntry.id, request)
        then:
            def ex = thrown(CommunicationProblemException)
            ex.sourceHttpCode == HttpStatus.BAD_REQUEST.value()
            ex.sourceErrorCodes == [ErrorCode.SYSTEM_DICTIONARY_ENTRY_ATTEMPT_UPDATE.name()] as Set
    }

    def "should get dictionary entry"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary())
            def dictionaryEntry = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        when:
            def response = dictionaryEntryManagementsClient.getDictionaryEntry(dictionaryEntry.id)
        then:
            response.statusCode == HttpStatus.OK
            DictionaryEntryManagementHelper.compare(dictionaryEntry, response.body)
    }

    def "should not get deleted dictionary entry"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary())
            def dictionaryEntry = saveDeletedDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
                deleted = true
            })
        when:
            dictionaryEntryManagementsClient.getDictionaryEntry(dictionaryEntry.id)
        then:
            def ex = thrown(ElementNotFoundException)
            ex.errorCode == ErrorCode.DICTIONARY_ENTRY_NOT_FOUND.name()
    }

    def "should not get dictionary entry for deleted dictionary"() {
        given:
            def dictionary = saveDeletedDictionary(DictionaryHelper.defaultDictionary().tap {
                deleted = true
            })
            def dictionaryEntry = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        when:
            dictionaryEntryManagementsClient.getDictionaryEntry(dictionaryEntry.id)
        then:
            def ex = thrown(ElementNotFoundException)
            ex.errorCode == ErrorCode.DICTIONARY_NOT_FOUND.name()
    }

    def "should delete dictionary entry"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary())
            def dictionaryEntry = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        when:
            def response = dictionaryEntryManagementsClient.deleteDictionaryEntry(dictionaryEntry.id)
        then:
            response.statusCode == HttpStatus.NO_CONTENT
            with(dictionaryEntryRepository.getDeletedDictionaryEntryById(dictionaryEntry.id)) {
                it.deleted
            }
    }

    def "should not delete system dictionary entry"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary().tap {
                system = true
            })
            def dictionaryEntry = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        when:
            dictionaryEntryManagementsClient.deleteDictionaryEntry(dictionaryEntry.id)
        then:
            def ex = thrown(CommunicationProblemException)
            ex.sourceHttpCode == HttpStatus.BAD_REQUEST.value()
            ex.sourceErrorCodes == [ErrorCode.SYSTEM_DICTIONARY_ENTRY_DELETION_ATTEMPT.name()] as Set
    }
}
