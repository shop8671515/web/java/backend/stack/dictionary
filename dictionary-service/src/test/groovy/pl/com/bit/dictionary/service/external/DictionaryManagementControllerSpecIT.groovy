package pl.com.bit.dictionary.service.external

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import pl.com.bit.common.exception.CommunicationProblemException
import pl.com.bit.common.exception.ElementNotFoundException
import pl.com.bit.dictionary.BaseITSpecification
import pl.com.bit.dictionary.feign.DictionaryManagementsClient
import pl.com.bit.dictionary.helper.DictionaryEntryHelper
import pl.com.bit.dictionary.helper.DictionaryHelper
import pl.com.bit.dictionary.helper.DictionaryManagementHelper
import pl.com.bit.http.problem.Problem
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryDetails
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryFilter
import pl.com.rszewczyk.stack.dictionary.api.model.ErrorCode

import java.util.function.Consumer

class DictionaryManagementControllerSpecIT extends BaseITSpecification {
    static final String BASE_URL = "/dictionaries"
    static final String DICTIONARY_ID_URL = "${BASE_URL}/{dictionaryId}"

    @Autowired
    DictionaryManagementsClient dictionaryManagementsClient

    def cleanup() {
        dictionaryRepository.deleteAll()
        namedObjectSnapRepository.deleteAll()
    }

    def "should create dictionary"() {
        given:
            def request = DictionaryManagementHelper.defaultDictionaryCreateRequest()
        when:
            def response = dictionaryManagementsClient.createDictionary(request)
        then:
            response.statusCode == HttpStatus.CREATED
            with(dictionaryRepository.getDictionaryById(response.body.id)) {
                DictionaryManagementHelper.compare(it, request)
                DictionaryManagementHelper.compare(it, response.body)
            }
    }

    def "should not create another dictionary with the same name"() {
        given:
            def dictionary = DictionaryHelper.defaultDictionary()
            saveDictionary(dictionary)
        and:
            def request = DictionaryManagementHelper.defaultDictionaryCreateRequest().tap {
                name = "test"
            }
        when:
            dictionaryManagementsClient.createDictionary(request)
        then:
            def ex = thrown(CommunicationProblemException)
            ex.sourceHttpCode == HttpStatus.BAD_REQUEST.value()
            ex.sourceErrorCodes == [ErrorCode.DICTIONARY_ALREADY_EXISTS.name()] as Set
    }

    def "should update dictionary"() {
        given:
            def dictionary = DictionaryHelper.defaultDictionary()
            dictionary = saveDictionary(dictionary)
        and:
            def request = DictionaryManagementHelper.defaultDictionaryUpdateRequest().tap {
                version = dictionary.version
            }
        when:
            def response = dictionaryManagementsClient.updateDictionary(dictionary.id, request)
        then:
            response.statusCode == HttpStatus.OK
            with(dictionaryRepository.getDictionaryById(response.body.id)) {
                DictionaryManagementHelper.compare(it, request)
                DictionaryManagementHelper.compare(it, response.body)
            }
    }

    def "should delete dictionary"() {
        given:
            def dictionary = DictionaryHelper.defaultDictionary()
            dictionary = saveDictionary(dictionary)
        when:
            def response = dictionaryManagementsClient.deleteDictionary(dictionary.id)
        then:
            response.statusCode == HttpStatus.NO_CONTENT
            with(dictionaryRepository.getDeletedById(dictionary.id)) {
                assert it.deleted
            }
    }

    def "should not delete system dictionary"() {
        given:
            def dictionary = DictionaryHelper.defaultDictionary().tap {
                system = true
            }
            dictionary = saveDictionary(dictionary)
        when:
            dictionaryManagementsClient.deleteDictionary(dictionary.id)
        then:
            def ex = thrown(CommunicationProblemException)
            ex.sourceHttpCode == HttpStatus.BAD_REQUEST.value()
            ex.sourceErrorCodes == [ErrorCode.SYSTEM_DICTIONARY_ATTEMPT_DELETION.name()] as Set
    }

    def "should get dictionary"() {
        given:
            def dictionary = DictionaryHelper.defaultDictionary()
            dictionary = saveDictionary(dictionary)
        when:
            def response = dictionaryManagementsClient.getDictionary(dictionary.id)
        then:
            response.statusCode == HttpStatus.OK
            with(dictionaryRepository.getDictionaryById(dictionary.id)) {
                DictionaryManagementHelper.compare(it, response.body)
            }
    }

    def "should not get deleted dictionary"() {
        given:
            def dictionary = saveDeletedDictionary(DictionaryHelper.defaultDictionary().tap {
                deleted = true
            })
        when:
            dictionaryManagementsClient.getDictionary(dictionary.id)
        then:
            def ex = thrown(ElementNotFoundException)
            ex.errorCode == ErrorCode.DICTIONARY_NOT_FOUND.name()
    }

    def "should get dictionary with some entries"() {
        given:
            def dictionary = saveDictionary(DictionaryHelper.defaultDictionary())
        and:
            def dictionaryEntry1 = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
            def dictionaryEntry2 = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
            def dictionaryEntry3 = saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                it.dictionary = dictionary
            })
        when:
            def response = dictionaryManagementsClient.getDictionary(dictionary.id)
        then:
            response.statusCode == HttpStatus.OK
            with(dictionaryRepository.getDictionaryById(dictionary.id)) {
                DictionaryManagementHelper.compare(it, response.body)
            }
    }

    def "should filter dictionaries by categories"() {
        given:
            def dictionary1 = saveDictionary(DictionaryHelper.defaultDictionary().tap {
                categories = ['test', 'abc', '123']
            })
            def dictionary2 = saveDictionary(DictionaryHelper.defaultDictionary().tap {
                categories = ['inny', '456', 'cde']
            })
            def dictionary3 = saveDictionary(DictionaryHelper.defaultDictionary().tap {
                categories = ['inna', '1a2', 'efg']
            })
        expect:
            findDictionaries { f -> f.categories = ['test', 'cde', 'inna'] }.totalElements == 3
            findDictionaries { f -> f.categories = ['abc', '1a2'] }.totalElements == 2
            findDictionaries { f -> f.categories = ['efg'] }.totalElements == 1
            findDictionaries { f -> f.categories = ['no'] }.totalElements == 0
    }

    def findDictionaries(Consumer<DictionaryFilter> filterConfig) {
        DictionaryFilter dictionaryFilter = new DictionaryFilter()
        filterConfig.accept(dictionaryFilter)

        def response = dictionaryManagementsClient.findDictionaries(dictionaryFilter, pageable)

        response.body
    }
}
