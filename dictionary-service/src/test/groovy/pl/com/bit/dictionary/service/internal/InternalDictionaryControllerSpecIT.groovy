package pl.com.bit.dictionary.service.internal

import org.springframework.beans.factory.annotation.Autowired
import pl.com.bit.dictionary.BaseITSpecification
import pl.com.bit.dictionary.feign.DictionaryInternalClient
import pl.com.bit.dictionary.helper.DictionaryEntryHelper
import pl.com.bit.dictionary.helper.DictionaryHelper
import pl.com.rszewczyk.stack.dictionary.api.model.DictionaryEntryFilter

import java.util.function.Consumer

class InternalDictionaryControllerSpecIT extends BaseITSpecification {

    @Autowired
    DictionaryInternalClient dictionaryInternalClient

    def setup () {
        namedObjectSnapRepository.deleteAll()
        dictionaryEntryRepository.deleteAll()
        dictionaryRepository.deleteAll()
    }

    def "should find all entries by dictionary id"() {
        given:
            def dictionary1 = saveDictionary(DictionaryHelper.defaultDictionary())
            def dictionary2 = saveDictionary(DictionaryHelper.defaultDictionary())
        and:
            saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                dictionary = dictionary1
            })
            saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                dictionary = dictionary1
            })
            saveDictionaryEntry(DictionaryEntryHelper.defaultDictionaryEntry().tap {
                dictionary = dictionary2
            })
        expect:
            findDictionaryEntries { f -> f.dictionaryId = dictionary1.id }.size() == 2
            findDictionaryEntries { f -> f.dictionaryId = dictionary2.id }.size() == 1
            findDictionaryEntries { f -> f.dictionaryId = UUID.randomUUID() }.size() == 0
    }

    def findDictionaryEntries(Consumer<DictionaryEntryFilter> filterConfig) {
        DictionaryEntryFilter filter = new DictionaryEntryFilter()
        filterConfig.accept(filter)

        def response = dictionaryInternalClient.getInternalDictionariesEntries(filter)

        response.body.entryList
    }
}
